package com.atlassian.stash.plugins.spotify.feature.client;

import java.io.IOException;

import com.atlassian.fugue.Either;
import com.atlassian.stash.plugins.spotify.feature.client.model.SpotifyModel;
import com.atlassian.stash.plugins.spotify.feature.client.model.Track;

import org.json.JSONException;

/**
 * Entry point for accessing the Spotify Web API (https://developer.spotify.com/technologies/web-api/).
 */
public interface SpotifyClient
{
    public Either<String, Iterable<Track>> search(final String query) throws JSONException, IOException;

    public Either<String, SpotifyModel> lookup(final String spotifyId) throws JSONException, IOException;
}
