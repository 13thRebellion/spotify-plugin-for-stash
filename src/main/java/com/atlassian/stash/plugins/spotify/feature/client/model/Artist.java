package com.atlassian.stash.plugins.spotify.feature.client.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="artist")
public class Artist extends SpotifyModel
{
    public Artist(final String href, final String name)
    {
        super(href, name);
    }
}
