package com.atlassian.stash.plugins.spotify;

import com.atlassian.event.api.EventListener;
import com.atlassian.stash.event.pull.PullRequestOpenedEvent;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestUpdateRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Listens for new pull requests being requested that may have spotify track metadata embedded within their description.
 */
public class PullRequestListener
{
    private static final String SPOTIFY_DESCRIPTION_REGEX = "\\[(.*)\\]\\(https://embed\\.spotify\\.com\\)";

    private final PullRequestService pullRequestService;
    private final TrackAssociationManager trackAssociationManager;
    private final Pattern pattern;

    public PullRequestListener(final PullRequestService pullRequestService, final TrackAssociationManager trackAssociationManager)
    {
        this.pullRequestService = pullRequestService;
        this.trackAssociationManager = trackAssociationManager;
        this.pattern = Pattern.compile(SPOTIFY_DESCRIPTION_REGEX);
    }

    @EventListener
    public void onPullRequestOpened(PullRequestOpenedEvent event)
    {
        final String originalDescription = event.getPullRequest().getDescription();

        // Is there an embedded spotify link in there? If so, extract and remove it.
        Matcher matcher = pattern.matcher(originalDescription);
        if (!matcher.find())
        {
            return;
        }

        final String spotifyTrackId = matcher.group(1);
        final String newDescription = matcher.replaceAll("");

        PullRequestUpdateRequest.Builder builder = new PullRequestUpdateRequest.Builder(event.getPullRequest(), event.getPullRequest().getVersion());
        PullRequestUpdateRequest prur =
                builder
                    .description(newDescription)
                    .title(event.getPullRequest().getTitle())
                    .build();

        PullRequest updatedPullRequest = pullRequestService.update(prur);

        // Associate the track ID with the Pull Request
        trackAssociationManager.associate(spotifyTrackId, updatedPullRequest);
    }
}
