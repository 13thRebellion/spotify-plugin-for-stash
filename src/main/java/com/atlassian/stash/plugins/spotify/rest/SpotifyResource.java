package com.atlassian.stash.plugins.spotify.rest;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.atlassian.fugue.Either;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.plugins.spotify.feature.client.SpotifyClient;
import com.atlassian.stash.plugins.spotify.feature.client.model.Track;

import com.google.common.collect.Iterables;

import org.json.JSONException;

/**
 * REST Resource that proxies the Spotify Metadata API (ref: https://developer.spotify.com/technologies/web-api/).
 */
@Path("/")
public class SpotifyResource
{
   private final SpotifyClient spotifyClient;

    public SpotifyResource(final SpotifyClient spotifyClient)
    {
        this.spotifyClient = spotifyClient;
    }

    @GET
    @Path("/track")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response trackSearch(@QueryParam("filter") String filter) throws IOException, JSONException
    {
        Either<String, Iterable<Track>> maybeResults = spotifyClient.search(filter);
        if (maybeResults.isLeft())
        {
            return Response.serverError().entity(maybeResults.left().get()).build();
        }

        return Response.ok(new Values(maybeResults.right().get(), filter)).build();
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public class Values
    {
        public Values(Iterable<Track> tracks, String filter)
        {
            this.values = tracks;
            this.size = Iterables.size(tracks);
            this.limit = 10;
            this.isLastPage = true;
            this.start = 0;
            this.filter = filter;
        }

        @XmlElement(name = "filter")
        public String filter;

        @XmlElement(name = "start")
        public int start;

        @XmlElement(name = "values")
        public Iterable<Track> values;

        @XmlElement(name = "size")
        public int size;

        @XmlElement(name = "limit")
        public int limit;

        @XmlElement(name = "isLastPage")
        public boolean isLastPage;
    }
}