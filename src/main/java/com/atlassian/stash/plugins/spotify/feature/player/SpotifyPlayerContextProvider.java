package com.atlassian.stash.plugins.spotify.feature.player;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Maybe;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.stash.plugins.spotify.TrackAssociationManager;
import com.atlassian.stash.plugins.spotify.feature.client.SpotifyClient;
import com.atlassian.stash.plugins.spotify.feature.client.model.SpotifyModel;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inserts the spotify track association associated with the current pull request, if it exists.
 */
public class SpotifyPlayerContextProvider implements ContextProvider
{
    private static final Logger log = LoggerFactory.getLogger(SpotifyPlayerContextProvider.class);
    private static final String PULL_REQUEST_URL_REGEX = ".*/projects/(.+)/repos/(.+)/pull-requests/(\\d+)/?.*";

    private final SpotifyClient spotifyClient;
    private final TrackAssociationManager trackAssociationManager;
    private final PullRequestService pullRequestService;
    private final RepositoryService repositoryService;
    private final StashAuthenticationContext stashAuthenticationContext;
    private final Pattern pattern;

    public SpotifyPlayerContextProvider(final SpotifyClient spotifyClient,
                                        final TrackAssociationManager trackAssociationManager,
                                        final PullRequestService pullRequestService,
                                        final RepositoryService repositoryService,
                                        final StashAuthenticationContext stashAuthenticationContext)
    {
        this.spotifyClient = spotifyClient;
        this.trackAssociationManager = trackAssociationManager;
        this.pullRequestService = pullRequestService;
        this.repositoryService = repositoryService;
        this.stashAuthenticationContext = stashAuthenticationContext;
        this.pattern = Pattern.compile(PULL_REQUEST_URL_REGEX);
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException
    {
        // no-op
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        Map<String, Object> contextMap = Maps.newHashMap();

        // HACK: There doesn't appear to be any way to identify the pull request that is being viewed from here, other
        // than by reversing the URL into its constituent parts.
        HttpServletRequest request = (HttpServletRequest)context.get("request");

        Matcher urlMatcher = pattern.matcher(request.getRequestURI());
        if (!urlMatcher.find())
            return contextMap;

        final String projectKey = urlMatcher.group(1);
        final String repoSlug = urlMatcher.group(2);
        final Long pullRequestId = Long.valueOf(urlMatcher.group(3));

        // Locate the repo hosting the PR
        Repository repo = repositoryService.getBySlug(projectKey, repoSlug);
        if (repo == null)
        {
            log.warn(String.format("Unable to locate repo %s in project %s", repoSlug, projectKey));
            return contextMap;
        }

        // Load the PR itself
        PullRequest pr = pullRequestService.getById(repo.getId(), pullRequestId);

        StashUser currentUser = stashAuthenticationContext.getCurrentUser();
        final Integer currentUserId = currentUser == null ? -1 : currentUser.getId();

        StashUser author = pr.getAuthor().getUser(); // Pull Request Author cannot be null/anonymous
        Integer authorId = author.getId();

        if (authorId.equals(currentUserId))
        {
            contextMap.put("isAuthor", true);
        }

        Collection<PullRequestParticipant> filter = Collections2.filter(pr.getReviewers(), new Predicate<PullRequestParticipant>()
        {
            @Override
            public boolean apply(@Nullable PullRequestParticipant pullRequestParticipant)
            {
                StashUser user = pullRequestParticipant.getUser();
                return user.getId().equals(currentUserId);
            }
        });
        if (filter.size() > 0)
        {
            contextMap.put("isReviewer", true);
        }

        // Any music there?
        Maybe<String> association = trackAssociationManager.getAssociation(pr);
        if (association.isDefined())
        {
            try
            {
                Either<String, SpotifyModel> maybeTrack = spotifyClient.lookup(association.get());
                if (maybeTrack.isRight())
                {
                    contextMap.put("trackInfo", maybeTrack.right().get());
                }
                else
                {
                    log.error(maybeTrack.left().get());
                }
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
        }

        return contextMap;
    }
}
