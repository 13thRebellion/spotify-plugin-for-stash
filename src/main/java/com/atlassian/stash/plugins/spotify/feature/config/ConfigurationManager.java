package com.atlassian.stash.plugins.spotify.feature.config;

/**
 * Persists the add-on configuration.
 */
public interface ConfigurationManager
{
    public void setTerritory(final String territory);
    public String getTerritory();
}
