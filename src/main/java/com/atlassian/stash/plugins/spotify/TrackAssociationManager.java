package com.atlassian.stash.plugins.spotify;

import com.atlassian.fugue.Maybe;
import com.atlassian.stash.pull.PullRequest;

/**
 *
 */
public interface TrackAssociationManager
{
    public Maybe<String> getAssociation(final PullRequest pullRequest);
    public void associate(final String trackId, final PullRequest pullRequest);

}
