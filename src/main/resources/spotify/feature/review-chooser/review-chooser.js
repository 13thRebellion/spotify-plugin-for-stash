/**
 * Adds an additional input field to the 'Create Pull Request' form, and manipulates the form data sent to the server
 * immediately before Form submission.
 */
define("spotify/feature/review-chooser", [
    "aui",
    "jquery",
    "spotify/widget/track-selector",
    "exports"
], function(AJS, $, TrackSelector, exports) {

    var $form;
    var $trackSelector;

    exports.onReady = function(selector$) {

        $form = selector$;

        var inputHtml = com.atlassian.stash.plugins.spotify.trackField({});

        $(".pull-request-details", selector$).append(inputHtml);

        $trackSelector = $("#spotify-trackId", selector$);
        new TrackSelector($trackSelector, {});
    };

    exports.onPresubmit = function(button$) {

        // Only need to manipulate the form submission if a track has actually been selected.
        var track = $trackSelector.val();
        if (track)
        {
            var $description = $("#description", $form);

            // Get the selected value from the spotify field, and append to the description.
            var md = "[" + track + "](https://embed.spotify.com)";
            $description.val($description.val() + "\r\n\r\n" + md);
        }
    }
});

// Require ourselves until I work out the right way to structure things.
AJS.toInit(function() {
    var reviewChooser = require("spotify/feature/review-chooser");
    var $form = AJS.$(".pull-request-create-form");
    reviewChooser.onReady($form);

    // Find the Create Pull Request button
    var $create = AJS.$("#submit-form", $form);
    $create.on("click", reviewChooser.onPresubmit);

});