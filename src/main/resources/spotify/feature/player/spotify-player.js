define("spotify/feature/player", [
    "aui",
    "jquery",
    "spotify/widget/selector-form",
    "exports"
], function(AJS, $, SelectorForm, exports) {

    var submitCallback = function(tracks) {
        $(".spotify-trackInfo").html(com.atlassian.stash.plugins.spotify.trackInfo({trackInfo: tracks[0], who: "You"}));

        // What's the current Project key, repo slug and PR ID?
        var prSection$ = $("section#content");
        var projectKey = prSection$.data("projectkey");
        var repoSlug = prSection$.data("reposlug");
        var prId = prSection$.data("pullrequestid");

        // Do the update
        var url = AJS.contextPath() + "/rest/spotify/1.0/associations/projects/" + encodeURIComponent(projectKey) + "/repos/" + encodeURIComponent(repoSlug) + "/pull-requests/" + encodeURIComponent(prId);
        $.ajax(url, {
            contentType: "text/plain",
            data: tracks.length > 0 ? tracks[0].href : "",
            type: "put",
            success: function(data) {
                AJS.log("Spotify track association updated asynchronously");
            },
            error: function(textStatus, errorThrown, jqXhr) {
                AJS.log("Failed to update track association: " + textStatus + ", " + errorThrown);;
            }
        });
    };

    var form = new SelectorForm(submitCallback);

    exports.onReady = function(changeButton$) {

        changeButton$.click(function(event) {
            form.show();
        });
    };
});

AJS.toInit(function() {

    var changeButton$ = AJS.$("#spotify-change-track");
    if (changeButton$.length > 0) {
        var spotifyPlayer = require("spotify/feature/player");
        spotifyPlayer.onReady(changeButton$);
    }
});