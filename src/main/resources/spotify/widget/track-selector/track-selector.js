define("spotify/widget/track-selector", [
    'aui',
    'jquery',
    'widget/searchable-multi-selector'
], function(
    AJS,
    $,
    SearchableMultiSelector) {

        function TrackMultiSelector($field, options) {
            SearchableMultiSelector.call(this, $field, options);
        }

        $.extend(true, TrackMultiSelector.prototype, SearchableMultiSelector.prototype, {
            defaults: {
                url: AJS.contextPath() + "/rest/spotify/1.0/track",
                selectionTemplate: function(track) {
                    return com.atlassian.stash.plugins.spotify.selectedTrack({track: track});
                },
                resultTemplate: function(track) {
                    return com.atlassian.stash.plugins.spotify.trackSelector({track: track, contextPath: AJS.contextPath()});
                },
                generateId: function(track) {
                    return track.href;
                }
            }
        });

        return TrackMultiSelector;
    }
);