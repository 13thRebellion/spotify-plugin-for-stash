define("spotify/widget/selector-form", [
    "aui",
    "jquery",
    "spotify/widget/track-selector"
], function(AJS, $, TrackSelector) {

    return (function() {

        var dialog;
        var trackSelector;

        var SelectorForm = function(submitCallback) {
            dialog = new AJS.Dialog(500, 250);
            dialog.addHeader("Select a Track");
            dialog.addPanel("", com.atlassian.stash.plugins.spotify.selectForm({}), "spotify-dialog-panel");

            var hideFunction = this.hide;

            dialog.addSubmit("Save", function() {

                var tracks = trackSelector.getSelectedItems();
                if (submitCallback && (typeof submitCallback === "function")) {
                    submitCallback(tracks);
                }

                hideFunction();

            });
            dialog.addCancel("Cancel", hideFunction);
        };

        SelectorForm.prototype = {
            dialog: dialog,
            show: function() {
                dialog.show();

                var $spotify = $(".spotify-dialog-panel #spotify-trackId");
                trackSelector = new TrackSelector($spotify, {});
            },
            hide: function() {
                dialog.hide();
            },
            remove: function() {
                dialog.remove();
            }
        };

        return SelectorForm;
    })();
});
