package it.com.atlassian.stash;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.stash.test.DefaultFuncTestData;
import com.atlassian.stash.test.runners.FuncTestSplitterRunner;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.page.StashLoginPage;
import com.atlassian.webdriver.stash.page.StashPage;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import it.com.atlassian.stash.rules.SessionCleanupRule;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.runner.RunWith;

@RunWith(FuncTestSplitterRunner.class)
public abstract class BaseFuncTest {

    protected final StashTestedProduct STASH = TestedProductFactory.create(StashTestedProduct.class);

    @Rule
    public SessionCleanupRule cleanupRule = new SessionCleanupRule();

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @Rule
    public final WindowSizeRule windowSizeRule = new WindowSizeRule();

    @AfterClass
    public static void clearBrowserCookies() {
        TestedProductFactory.create(StashTestedProduct.class).getTester().getDriver().manage().deleteAllCookies();
    }

    protected <P extends StashPage> P loginAsAdmin(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), page, args);
    }

    protected <P extends StashPage> P loginAsRegularUser(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getRegularUser(), DefaultFuncTestData.getRegularUserPassword(), page, args);
    }

    protected <P extends StashPage> P anonymous(Class<P> page, Object... args) {
        clearBrowserCookies();
        return STASH.visit(page, args);
    }

    protected <P extends StashPage> P loginAs(String username, String password, Class<P> page, Object... args) {
        clearBrowserCookies();
        return STASH.visit(StashLoginPage.class).login(username, password, page, args);
    }

}
