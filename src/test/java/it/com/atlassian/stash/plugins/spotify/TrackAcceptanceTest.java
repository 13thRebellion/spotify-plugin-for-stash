package it.com.atlassian.stash.plugins.spotify;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.stash.test.DefaultFuncTestData;
import com.atlassian.stash.test.RepositoryTestHelper;
import com.atlassian.stash.test.TestContext;
import com.atlassian.stash.user.Permission;
import com.atlassian.webdriver.stash.element.Select2;
import com.atlassian.webdriver.stash.page.PullRequestCreatePage;
import com.atlassian.webdriver.stash.page.PullRequestOverviewPage;
import com.atlassian.webdriver.utils.Check;

import com.google.common.base.Predicate;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import it.com.atlassian.stash.BaseFuncTest;

import static com.atlassian.stash.test.GitTestHelper.getGitPath;
import static com.atlassian.stash.test.ProcessTestHelper.execute;
import static org.junit.Assert.assertTrue;

public class TrackAcceptanceTest extends BaseFuncTest
{
    private static final String TEST_PR_AUTHOR = "prauthor";
    private static final String TEST_PR_REVIEWER = "prreviewer";
    private static final String TEST_ANOTHER_USER = "someguy";
    private static final String TEST_GROUP_NAME = "spotify-users";

    private static final String TEST_PROJECT_KEY = "SPOTIFY";
    private static final String TEST_REPO_NAME = "repo";

    private static final String TEST_SOURCE_BRANCH = "feature";
    private static final String TEST_DEST_BRANCH = "master";

    @Rule
    public final TestContext context = new TestContext();

    @Rule
    public final TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void init() throws Exception
    {
        context.user(TEST_PR_AUTHOR, "password")
               .user(TEST_PR_REVIEWER, "password")
               .user(TEST_ANOTHER_USER, "password")
               .group(TEST_GROUP_NAME, TEST_PR_AUTHOR, TEST_PR_REVIEWER, TEST_ANOTHER_USER)

               .project(TEST_PROJECT_KEY)

               .repository(TEST_PROJECT_KEY, TEST_REPO_NAME)
               .repositoryPermissionForGroup(TEST_PROJECT_KEY, TEST_REPO_NAME, TEST_GROUP_NAME, Permission.REPO_WRITE);

        createGitRepo();
    }

    private void createGitRepo() throws IOException, URISyntaxException
    {
        File repoDir = tempFolder.newFolder("repo");

        execute(repoDir, getGitPath(), "init");
        FileUtils.writeStringToFile(new File(repoDir, "README.md"), "This is a test");
        execute(repoDir, getGitPath(), "add", ".");
        execute(repoDir, getGitPath(), "commit", "-m", "Initial commit");
        execute(repoDir, getGitPath(), "checkout", "-b", TEST_SOURCE_BRANCH);
        FileUtils.writeStringToFile(new File(repoDir, "password.txt"), "letmein");
        execute(repoDir, getGitPath(), "add", ".");
        execute(repoDir, getGitPath(), "commit", "-m", "Adding password file to repo");


        RepositoryTestHelper.pushRep(repoDir, DefaultFuncTestData.getBaseURL(), DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), TEST_PROJECT_KEY, TEST_REPO_NAME);
    }

    @Test
    public void testCreatePullRequestWithTrack() throws Exception
    {
        PullRequestCreatePage prPage = loginAsAdmin(PullRequestCreatePage.class, TEST_PROJECT_KEY, TEST_REPO_NAME);

        prPage.getSourceRepositorySelector().open().selectItemByName(TEST_PROJECT_KEY + "/" + TEST_REPO_NAME);
        prPage.getSourceBranchSelector().open().selectItemByName(TEST_SOURCE_BRANCH);

        Select2 trackSelector = prPage.getDetails().find(By.id("s2id_spotify-trackId"), Select2.class);
        trackSelector.add("Inhaler", new Predicate<PageElement>()
        {
            @Override
            public boolean apply(PageElement pageElement)
            {
                boolean correctArtist = pageElement.find(By.className("spotify-artist")).getText().equals("Foals");
                boolean correctTrack = pageElement.find(By.className("spotify-track")).getText().equals("Inhaler");
                return correctArtist && correctTrack;
            }
        });

        PullRequestOverviewPage prOverview = prPage.clickSubmit();
        final Long id = prOverview.getPullRequestId();

        assertTrue(prOverview.isHere());

        WebDriver driver = STASH.getTester().getDriver();

        assertTrue(Check.elementExists(By.xpath("//a[@href=\"spotify:track:3tRqWoZuQtOfG54Vd6nNdq\"]"), driver));
    }


}
